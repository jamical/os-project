package com.dark.rs2.content.bot;

/**
 * Holds the trivia Data
 * @author Daniel
 *
 */
public enum BotData {	
	  DATA_1("Who is the CEO of Dark-Asylum?", "jamian", "goten"),
	  DATA_2("What is the Answer to the Ultimate Question of Life?", "42"),
	  DATA_3("How many NFL teams are there", "32"),
	  DATA_4("How many unique barrows items are there?", "25"),
	  DATA_5("How many developers are there of Dark-Asylum?", "3", "three"),
	  DATA_6("Which NPC has four enourmous tentacles?", "Kraken", "kraken"),
	  DATA_8("I am a rock that turns into a crab what am i called?", "Rock crab", "Rock crabs"),
	  DATA_9("What is the highest level a stat can reach?", "99"),
	  DATA_10("What command opens up the forums?", "::forums", "::forum"),
	  DATA_11("In Dark-Asylum how many NPCs drop the Chaos Elemental pet?", "2", "two"),
	  DATA_12("What monster drops the Abyssal Whip?", "abyssal demon", "abby demon"),
	  DATA_13("Corporeal Beast drops how many onyx bolts(e)?", "175"),
	  DATA_14("Tanzanite fang turns into what when cut with a chisel?", "toxic Blowpipe", "blowpipe"),
	  DATA_15("What is the name of the NPC that drops Wyvern bones?", "skeletal Wyvern", "skeletal", "wyverns"),
	  DATA_16("What command opens up the Donator page?", "::donate"),
	  DATA_17("What is the max combat level you can achieve in Dark-Asylum?", "126"),
	  DATA_18("On which day is the club going up?", "tuesday", "Tuesday"),
	  DATA_19("Which of the barrows warriors is based on magic?","Ahrim", "ahrim"),
	  DATA_20("What is the required fishing level for sharks?","76", "76"),
	  DATA_21("What is the smithing level required to create a DFS?", "90"),
	  DATA_22("Coke Or Pepsi?","coke", "pepsi"),
	  DATA_23("How many waves is fight caves in Dark-Asylum?","fifteen", "15"),
	  DATA_24("Which NPC drops Flippers?", "sea troll queen", "stq"),
	  DATA_25("How much special attack does Magic Short Bow require?", "55%", "55"),
	  DATA_26("Berserker ring is dropped by which NPC?","Dagannoth King Rex", "Dagannoth Rex", "Rex"),
	  DATA_27("How many friends can fit on the friends list?", "200"),
	  DATA_28("Type the alphabet backwards", "zyxwvutsrqponmlkjihgfedcba"),
	  DATA_29("Which godwars boss drops tassets?", "bandos"),
	  DATA_30("Where are spiritual mages found?", "Godwars", "Godwars dungeon"),
	  DATA_31("What is the required prayer level for Smite?", "52"),
	  DATA_32("Toxic blowpipe is a drop from what NPC?", "Zulrah"),
	  DATA_33("Where does ::train take you?", "Rock Crabs"),
	  DATA_34("Which NPC teleports you to rune essence mine", "Mage of zamorak", "Zamorak mage"),
	  DATA_35("What is the required crafting level to make an Amulet of Fury", "90"),
	  DATA_36("Who won Superbowl XI", "raiders", "oakland raiders"),
	  DATA_37("What year was Oldschool Runescape released", "2013"),
	  DATA_38("Who started from the bottom?", "drake"),
	  DATA_39("How many bank booths are in edgeville bank?", "4", "four"),
	  DATA_40("What slayer level is required to kill Kraken?", "87", "87"),
	  DATA_41("What is the required attack level to wield a godsword?", "75"),
	  DATA_42("How many barrows brothers are there?", "6"),
	  DATA_44("How much does regular membership cost?", "$5", "5 dollars", "5"),
	  DATA_45("How many donator ranks are there?", "4", "four"),
	  DATA_46("What game is wildly addictive?", "Dark-Asylum", "this game"),
	  DATA_47("What uses its tentacles for Violation", "Hentai"),
	  DATA_48("Who won the 2006 FIFA world cup?", "italy"),
	  DATA_49("Where did the lore of Dark-Asylum come from?", "asylum"),
	  DATA_50("Name one of Dark-Asylum's developers", "Goten", "", "Jamian"),
	  DATA_51("How many NBA teams are there?", "30"),
	  DATA_52("How many prayers are there?", "26", "twenty six", "twenty-six"),
	  DATA_53("What is the name of this server?", "dark-asylum", "dark asylum"),
	  DATA_54("What is the Woodcutting level required to wield a dragon hatchet?", "61"),	  
	  DATA_55("Which Dark-Asylum NPC is the strongest", "corp", "corporeal beast"),
	  DATA_56("Where is home in Dark-Asylum?", "nardah"),
	  DATA_57("What is the name of the NPC you can get skillcapes from?", "wise old man"),
	  DATA_58("What color party hat does the wise old man wear?", "blue"),
	  DATA_59("What skill involves the murder of many different NPC?", "slayer"),
	  DATA_60("What skill involves buring logs?", "firemaking"),
	  DATA_61("What is the required defence level to wear dragon armour?", "60", "sixty"),
	  DATA_62("What NPC allows you to reset combat stats in Dark Asylum?", "genie"),
	  DATA_63("What minigame offers void armour as a reward?", "pest control"),
	  DATA_64("What shield is the strongest in the game?", "elysian"),
	  DATA_65("How many Thieving stalls are there at home?", "3", "three"),
	  DATA_66("Who is the website developer?", "alex", "goten"),	  
	  DATA_67("Who thought this was america?", "randy", "randy marsh"),	  
	  DATA_68("How much money does normal membership cost?", "$5", "5"),  
	  DATA_69("What is the best crossbow in game?", "armadyl crossbow", "acb"),	  
	  DATA_70("What strength level is required to wield the granite maul", "50", "fifty"),
	  DATA_71("How many letters are in the word 'Dark-Asylum'?", "10", "ten"),  
	  DATA_72("What level magic does the spell High Alchemy require?", "55", "fifty-five", "fifty five"),
	  DATA_73("Who is the community manager?", "soft", "Soft", "Thomas"),
	  DATA_74("On a scale of 1 to 10, how often do you vote on scales of 1 to 10", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"),
	  DATA_75("Who is the best YouTuber on Dark-Asylum", "jamian", "bigboy", "chris", "views"),
	  DATA_76("Type Dark-Asylum backwards!", "mulysa-krad", "mulysa krad"),
	  DATA_77("How many questions are on this list?", "84", "how would i know that"),
	  DATA_78("How many Marks of Grace do you need for full Purple Graceful?", "300", "3 hunna"),
	  DATA_79("Thanks for playing the server!", "np", "no problem", "nothing better to do"),
	  DATA_80("Who is the greatest rapper of all time?", "tupac", "2pac", "biggie", "biggie smalls"),
	  DATA_81("Who is the gereatest American football player of all time?", "jerry rice", "Jerry Rice"),
	  DATA_82("Who is the gereatest Basketball player of all time?", "michael jordan", "Michael Jordan", "micheal jordan"),
	  DATA_83("True or False, if you get a Smoke devil task you can do the boss.", "True", "true"),
	  DATA_84("True or False, you can't have more then 1 clue.", "false", "False"),
	  DATA_85("What command gives you the option to Vote for the server, bringing in more players?", "vote", "::vote"),
	  DATA_86("Complete this anagram; ovte", "vote"),
	  DATA_87("Complete this anagram; drsa yalkmu", "dark asylum"),
	  DATA_88("Complete this anagram; coppalyesa", "apocalypse", "Apocalypse"),
	  DATA_89("Complete this anagram; ndpaa", "panda"),
	  DATA_90("Which npc exchanges jars for points?", "old man ral", "Old Man Ral")

	  ;
	
	private final String question;
	private final String[] answers;
	
	private BotData(String question, String... answers) {
		this.question = question;
		this.answers = answers;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public String[] getAnswers() {
		return answers;
	}
	
}
