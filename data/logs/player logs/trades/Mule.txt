
---2016/08/01 22:56:55---
Mule has received 1 Highwayman Mask from Zulrah
Mule has received 360 Goat Horn Dust from Zulrah
Mule has received 400 Raw Monkfish from Zulrah
Mule has received 1 Cat Mask from Zulrah
Mule has received 120 Oak Plank from Zulrah
Mule has received 1 Penguin Mask from Zulrah
Mule has received 200 Teak Plank from Zulrah
Mule has received 1 Black Cavalier from Zulrah
Mule has received 2 Flared Trousers from Zulrah
Mule has received 1 Black Skirt (G) from Zulrah
Mule has received 1 Zamorak Platebody from Zulrah
Mule has received 1 Pink Elegant Legs from Zulrah
Mule has received 1 Armadyl Stole from Zulrah
Mule has received 1 Pith Helmet from Zulrah
Mule has received 4 Black Wizard Hat (G) from Zulrah
Mule has received 1 Saradomin Platelegs from Zulrah
Mule has received 4 Jar Of Dirt from Zulrah
Mule has received 1 Bandos Stole from Zulrah
Mule has received 1 Guthix Platebody from Zulrah
Mule has received 1 Guthix Kiteshield from Zulrah
Mule has received 3431 Flax from Zulrah
Mule has received 1 Decorative Armour from Zulrah
Mule has received 1 Decorative Armour from Zulrah
Mule has received 1 Decorative Armour from Zulrah
Mule has received 1 Guthix Page 1 from Zulrah
Mule has received 1 Blue Elegant Blouse from Zulrah
Mule has received 400 Gold Ore from Zulrah
Mule has received 1 Rune Platebody (T) from Zulrah

---2016/08/02 00:22:37---
Mule has received 1 Kq Head from Purerangsaw

---2016/08/02 00:23:28---
Mule has received 1 Abyssal Head from Purerangsaw

---2016/08/02 00:23:54---
Mule has received 2 Kq Head from Purerangsaw

---2016/08/02 04:53:39---
Mule has received 15 Casket (Hard) from Zulrah
Mule has received 1 Brown Headband from Zulrah
Mule has received 1 Rune Axe from Zulrah

---2016/08/02 04:57:42---
Mule has received 1 Saradomin Bracers from Zulrah
Mule has received 1 Rune Sq Shield from Zulrah
Mule has received 1 Light Infinity Colour Kit from Zulrah
Mule has received 3 Casket (Hard) from Zulrah
Mule has received 1 Saradomin Coif from Zulrah
Mule has received 1 Red Boater from Zulrah
Mule has received 1 Magic Shortbow from Zulrah

---2016/08/02 05:03:43---
Mule has received 6 Casket (Hard) from Zulrah

---2016/08/02 05:05:03---
Mule has received 1 Zamorak Kiteshield from Zulrah
Mule has received 1 Rune Plateskirt from Zulrah
Mule has received 2 Rune Platebody from Zulrah
Mule has received 1 Enchanted Top from Zulrah
Mule has received 1 Black Helm (H5) from Zulrah
Mule has received 1 Rune Full Helm from Zulrah
Mule has received 1 Guthix Chaps from Zulrah
Mule has received 1 Blue D'Hide Body (G) from Zulrah
Mule has received 1 Rune Shield (H2) from Zulrah
Mule has received 1 Rune Kiteshield from Zulrah
Mule has received 1 Swordfish from Zulrah
Mule has received 1 Rune Longsword from Zulrah
Mule has received 1 Rune Platelegs from Zulrah
Mule has received 2 Blue D'Hide Chaps (T) from Zulrah
Mule has received 1 Rune Chainbody from Zulrah
Mule has received 1 Rune Shield (H4) from Zulrah
Mule has received 1 Rune Med Helm from Zulrah
Mule has received 1 Black D'Hide Vamb from Zulrah

---2016/08/03 02:10:57---
Mule has received 1 Rune Shield (H5) from Zulrah
Mule has received 1 White Cavalier from Zulrah
Mule has received 13 Casket (Hard) from Zulrah
Mule has received 1 Rune Plateskirt from Zulrah
Mule has received 1 Rune Platebody (G) from Zulrah
Mule has received 1 Rune Longsword from Zulrah

---2016/08/11 00:46:56---
Mule has given 4 Jar Of Dirt to Zulrah

---2016/08/11 17:25:11---
Mule has given 1 Mystery Box to Zulrah

---2016/08/11 19:43:02---
Mule has given 1 Magic Egg to Zulrahs

---2016/08/11 22:11:51---
Mule has received 1 Armadyl Stole from Zulrah
Mule has received 1 Red Cavalier from Zulrah
Mule has received 1 Zamorak Coif from Zulrah
Mule has received 1 Highwayman Mask from Zulrah
Mule has received 1 Blue Dragon Mask from Zulrah
Mule has received 1 Enchanted Hat from Zulrah
Mule has received 1 Red Dragon Mask from Zulrah
Mule has received 3 Rune Helm (H1) from Zulrah
Mule has received 1 Saradomin Cloak from Zulrah
Mule has received 2 Tan Cavalier from Zulrah
Mule has received 1 Clue Hunter Boots from Zulrah
Mule has received 2 Dark Infinity Colour Kit from Zulrah
Mule has received 1 Dark Cavalier from Zulrah
Mule has received 1 Smouldering Stone from Zulrah
Mule has received 1 Light Infinity Colour Kit from Zulrah
Mule has received 1 Black Cavalier from Zulrah
Mule has received 1 Saradomin Coif from Zulrah
Mule has received 1 Spirit Shield from Zulrah
Mule has received 1 Malediction Shard 3 from Zulrah
Mule has received 2 Zamorak Platelegs from Zulrah

---2016/08/18 11:44:16---
Mule has received 50000000 Coins from Purerangsaw

---2016/08/18 11:44:16---
Mule has given 1 Decorative Armour to Purerangsaw
Mule has given 1 Decorative Armour to Purerangsaw
Mule has given 1 Decorative Armour to Purerangsaw

---2016/08/18 11:44:29---
Mule has given 50000000 Coins to Zulrah

---2016/08/22 01:13:35---
Mule has received 7000000 Coins from Rat
Mule has received 1 Heavy Ballista from Rat

---2016/08/22 01:13:35---
Mule has given 1 Dragon Plate/Skirt Ornament Kit to Rat
